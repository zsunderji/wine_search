class AddLatitudeAndLongitudeToWines < ActiveRecord::Migration
  def change
    add_column :wines, :latitude, :float
    add_column :wines, :longitude, :float
  end
end
