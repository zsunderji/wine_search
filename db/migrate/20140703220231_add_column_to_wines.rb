class AddColumnToWines < ActiveRecord::Migration
  def change
    add_column :wines, :origin, :string
    add_column :wines, :serving_type, :string
  end
end