class RemoveIdsFromWines < ActiveRecord::Migration
  def change
    remove_column :wines, :restaurant_id, :integer
    remove_column :wines, :offering_id, :integer
  end
end
