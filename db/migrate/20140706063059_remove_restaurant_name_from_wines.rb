class RemoveRestaurantNameFromWines < ActiveRecord::Migration
  def change
    remove_column :wines, :restaurant_name, :string
  end
end


