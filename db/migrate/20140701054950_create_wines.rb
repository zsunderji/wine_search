class CreateWines < ActiveRecord::Migration
  def change
    create_table :wines do |t|
      t.string :name
      t.string :type
      t.references :restaurant, index: true
      t.references :offering, index: true

      t.timestamps
    end
  end
end
