class CreateOfferings < ActiveRecord::Migration
  def change
    create_table :offerings do |t|
      t.references :restaurant, index: true
      t.references :wine, index: true

      t.timestamps
    end
  end
end
