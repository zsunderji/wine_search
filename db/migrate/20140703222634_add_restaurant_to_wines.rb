class AddRestaurantToWines < ActiveRecord::Migration
  def change
    add_column :wines, :restaurant_name, :string
  end
end
