class Offering < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :wine
end
