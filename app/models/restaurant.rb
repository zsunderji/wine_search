  class Restaurant < ActiveRecord::Base

  belongs_to :user

  has_many :offerings
  has_many :wines, through: :offerings

  geocoded_by :address
  after_validation :geocode

  validates :name, :cuisine, :address, :avatar, presence: {message: "must exist"}

  self.per_page = 20

  has_attached_file :avatar, styles: { :medium => "300x300>", :thumb => "100x100>" },
                     default_url: ActionController::Base.helpers.asset_path("missing_:style.png"),
                     storage: :s3,
                     s3_host_name: "s3-us-west-2.amazonaws.com",
                     :s3_credentials =>{:bucket => "winesearch", :access_key_id => ENV['s3_access_key_id'], :secret_access_key => ENV['s3_secret_key_id']}

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  crop_attached_file :avatar, aspect: "1:1"

  def s3_credentials
    {:bucket => "winesearch", :access_key_id => ENV['s3_access_key_id'], :secret_access_key => ENV['s3_secret_key_id']}
  end

end