class Wine < ActiveRecord::Base

  has_many :offerings

  has_many :restaurants, through: :offerings

  validates :name, :origin, presence: {message: "must exist"}
  validates_uniqueness_of :name, :scope => :kind

  self.per_page = 15

  def self.accessible_attributes
    ["name", "kind", "origin", "serving_type"]
  end

  def self.import(file)
    CSV.foreach(file.path, headers: true, col_sep: "\t") do |row, index|
      wine = find_by_id(row[index]) || new
      wine.attributes = row.to_hash.slice(*accessible_attributes)
      wine.save!
      #wine = Wine.create! row.to_hash
      # wine.restaurants << restaurant
    end
  end

  def self.search(query)
    where("LOWER(name) like ?", "%#{query}%")
  end

end


