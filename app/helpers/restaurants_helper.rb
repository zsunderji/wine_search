module RestaurantsHelper

  def generate_map_json(restaurant)
    Gmaps4rails.build_markers(restaurant) do |restaurant, marker|
      marker.lat restaurant.latitude
      marker.lng restaurant.longitude
      link = link_to restaurant.title, restaurant_path(restaurant)
      marker.infowindow "#{restaurant.title}"
    end
  end
end

