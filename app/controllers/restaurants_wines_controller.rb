class RestaurantsWinesController < ApplicationController

  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]

  before_action :find_restaurant

  def new
    @wine = Wine.new
  end

  def create
    @wine = @restaurant.wines.new(wine_params)
    # @wine.user = current_user
    if @wine.save
      redirect_to root_path, notice: "#{@wine.name} was added to the database"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @wine.update_attributes
      redirect_to restaurant_wines_path
    else
      render :edit
    end
  end

  def show
  end

  def index
    if params[:search]
      @wines = Wine.search(params[:search]).order("created_at DESC")
    else
      @wines = Wine.all.order("created_at DESC")
    end
  end

  def destroy
    @wine.destroy 
  end

  def import
    Wine.import(params[:file])
    redirect_to root_path, notice: "Wines imported"
  end

  private

  def wine_params
    params.require(:wine).permit(:name, :kind, :origin, :serving_type)
  end

  def find_wine
    @wine = Wine.find params[:id]
  end

  def find_restaurant
    @restaurant = Restaurant.find(params[:restaurant_id])
  end

end
