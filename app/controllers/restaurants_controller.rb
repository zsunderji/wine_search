class RestaurantsController < ApplicationController

  helper_method :sort_column, :sort_direction

  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :find_restaurant, only: [:edit, :update, :show, :destroy]

  def new
    @restaurant = Restaurant.new
    @restaurant.wines.build
  end

  def create
    @restaurant = Restaurant.new restaurant_params
    @restaurant.user = current_user
    if @restaurant.save
      if params[:restaurant][:avatar]
        render :crop
      else
        redirect_to restaurant_path(@restaurant), notice: "Your restaurant was created!"
      end
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @restaurant.update_attributes restaurant_params
      if params[:restaurant][:avatar]
        render :crop
      else
        redirect_to restaurant_path(@restaurant), notice: "Your restaurant was created!"
      end
    else
      render :edit
    end
  end

  def destroy
    if @restaurant.destroy
      redirect_to root_path
    else
      notice = "We had some difficulties deleting the restaurant. Please try again"
    end
  end

  def show
    @wine = @restaurant.wines.build
    @hash = Gmaps4rails.build_markers(@restaurant) do |restaurant, marker|
      marker.lat restaurant.latitude
      marker.lng restaurant.longitude
      marker.json({ title: restaurant.name })
    end
  end

  def index
    @restaurants = Restaurant.paginate(:page => params[:page])
    @restaurants = @restaurants.order("#{sort_column} #{sort_direction}")
    if params[:search]
      @wines = Wine.search(params[:search]).order("created_at DESC").near([request.remote_ip, 20])
    else
      @wines = Wine.all.order("created_at DESC")
    end
    respond_to do |format|
      format.html
      format.js {render}
    end
  end


  def wines
    @wine = Wine.all
  end

  private

  def restaurant_params
    params.require(:restaurant).permit(:name,
                                       :cuisine, 
                                       :address,
                                       :avatar_original_w, 
                                       :avatar_original_h, 
                                       :avatar_box_w,
                                       :avatar_crop_x,
                                       :avatar_crop_y,
                                       :avatar_crop_w, 
                                       :avatar_crop_h, 
                                       :avatar_aspect,
                                       :city,
                                       :avatar,
                                       {wine_ids: []},
                                       {wine_attributes: [:name, :kind, :origin, :serving_type, :id]})

  end


  def find_restaurant
    @restaurant = Restaurant.find params[:id]
  end

  def sort_column
    Restaurant.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
