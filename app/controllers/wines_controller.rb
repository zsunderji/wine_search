class WinesController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :find_wine, only: [:show, :restaurants]

  helper_method :sort_column, :sort_direction

  def new
    @wine = Wine.new
  end

  def create
    @wine = Wine.new(wine_params)
    # @wine.user = current_user
    if @wine.save
      redirect_to root_path, notice: "#{@wine.name} was added to the database"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @wine.update_attributes
      redirect_to wines_path
    else
      render :edit
    end
  end

  def show
  end

  def restaurants 
    @wine.restaurants.any?
  end

  def index
    if params[:search]
      @wines_search = Wine.search(params[:search]).paginate(:page => params[:page])
                                                  .order("#{sort_column} " "#{sort_direction}")

                                                  
    else
      @wines_search = Wine.paginate(:page => params[:page])
                          .order("#{sort_column} " "#{sort_direction}")
    end
    respond_to do |format|
      format.html
      format.js { render }
    end
  end

  def destroy
    @wine.destroy 
  end

  def import
    Wine.import(params[:file])
    redirect_to root_path, notice: "Wines imported"
  end

  private

  def wine_params
    params.require(:wine).permit(:name, :kind, :origin, :serving_type)
  end

  def find_wine
    @wine = Wine.find params[:id]
  end

  def sort_column
    Wine.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
