// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require chosen-jquery
//= require underscore
//= require gmaps/google
//= require jquery.jcrop
//= require papercrop
//= require wines
//= require_tree .


// Full-text Search

// $(function () {
//     $('a[href="#main_search"]').on('click', function(event) {
//         event.preventDefault();
//         $('#main_search').addClass('open');
//         $('#main_search > form > input[type="search"]').focus();
//     });
    
//     $('#main_search, #main_search button.close').on('click keyup', function(event) {
//         if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
//             $(this).removeClass('open');
//         }
//     });
    
// });